package com.masuilab_isu.maptest;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PostLocations extends AsyncTask<String, Void, String> {

    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient client = new OkHttpClient();

    private String requestUrl;
    private String jsonParam;

    public PostLocations(String requestUrl,JSONObject locations){
        this.requestUrl = requestUrl;
        this.jsonParam = locations.toString();
    }

    @Override
    protected String doInBackground(String... params) {
        String response = postToServer(requestUrl,jsonParam);
        return response;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);

        System.out.println("---response---");
        System.out.println(response);
        System.out.println("---response---");
    }

    private String postToServer(String url, String json){
        RequestBody body = RequestBody.create(JSON, json);

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        try{
            Response response = client.newCall(request).execute();
            return response.body().string();
        }catch (IOException exception){
            System.out.println("---");
            System.out.println(exception);
            System.out.println("---");
        }
        return null;
    }

    public static JSONObject createLocations(String user,
                                             double latitude,
                                             double longitude,
                                             String time){
        try {
            JSONObject locations = new JSONObject();
            locations.put("userID", user);
            locations.put("latitude", latitude);
            locations.put("longitude", longitude);
            locations.put("time_stamp",time);
            return locations;
        }catch (JSONException jsonException){
            System.out.println("---JSONException---");
            System.out.println(jsonException);
            System.out.println("---JSONException---");
        }
        return null;
    }
}